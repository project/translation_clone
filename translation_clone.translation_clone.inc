<?php

/**
 * Implements hook_translation_clone_info().
 */
function translation_clone_translation_clone_info() {
  $info = array();

  // Find entity types with entity translation enabled.
  $all_entity_info = entity_get_info();
  $et_enabled = module_exists('entity_translation');
  if ($et_enabled) {
    $enabled_types = array_filter(variable_get('entity_translation_entity_types', array()));
    $et_bundles    = array();
    $_null = NULL;

    list($items,) = menu_router_build();
    _entity_translation_validate_path_schemes($_null, FALSE, $items);
    foreach ($all_entity_info as $entity_type => $entity_info) {

      if (in_array($entity_type, $enabled_types, TRUE) && $entity_info['fieldable']) {
        $et_info = &$entity_info['translation']['entity_translation'];
        _entity_translation_process_path_schemes($entity_type, $et_info);
        _entity_translation_validate_path_schemes($et_info['path schemes'], $entity_info['label']);

        // Translation can be enabled for the current entity only if it defines at
        // least one valid base path.
        foreach ($et_info['path schemes'] as $delta => $scheme) {
          if (!empty($scheme['base path'])) {
            $label = !empty($entity_info['label']) ? t($entity_info['label']) : $entity_type;
            $bundles = !empty($entity_info['bundles']) ? $entity_info['bundles'] : array($entity_type => array('label' => $label));
            foreach ($bundles as $bundle => $bundle_info) {
              if (entity_translation_enabled_bundle($entity_type, $bundle)) {
                $et_bundles[$entity_type . ':' . $bundle] = $label . ': ' . $bundle_info['label'];
              }
            }
            break;
          }
        }
      }
    }
    // Avoid bloating memory with unused data.
    drupal_static_reset('_entity_translation_validate_path_schemes');

    if ($et_bundles) {
      $info['translatables']['entity_translation'] = array(
        'name' => t('Entity translation'),
        'options' => $et_bundles,
        'operation' => 'translation_clone_entity_translations_operation',
        // Keep entity translation next to content translation.
        'weight' => -1,
        'description' => t('<a href="@translation_url">Entity types</a> configured for entity (field) translation.', array('@translation_url' => url('admin/config/regional/entity_translation'))),
      );
    }
  }

  if (isset($all_entity_info['node'])) {
    $t_bundles = array();
    foreach ($all_entity_info['node']['bundles'] as $bundle => $bundle_info) {
      if (locale_multilingual_node_type($bundle) && !($et_enabled && entity_translation_enabled_bundle('node', $bundle))) {
        $t_bundles[$bundle] = $bundle_info['label'];
      }
    }

    if ($t_bundles) {
      $info['translatables']['translation'] = array(
        'name' => t('Multilingual content'),
        'options' => $t_bundles,
        'operation' => 'translation_clone_content_translations_operation',
        // If content translation is in use at all, it will probably be more
        // important than entity translation as it covers nodes.
        'weight' => -2,
        'description' => t('<a href="@types_url">Node types</a> configured for translation under their publishing options.', array('@types_url' => url('admin/structure/types'))),
      );
    }
  }

  if ($groups = module_invoke_all('locale', 'groups')) {
    $info['translatables']['locale'] = array(
      'name' => t('Interface & user-defined strings'),
      'options' => $groups,
      'operation' => 'translation_clone_locale_translations_operation',
      // Since this element should always be present, show it first.
      'weight' => -3,
      'description' => t('Text translated at <a href="@translation_url">@translation_url</a>, plus any custom string overrides if they have been set.', array('@translation_url' => url('admin/config/regional/translate/translate'))),
    );
  }

  $info['translatables']['date_format_locale'] = array(
    'name' => t('Date formats'),
    'operation' => 'translation_clone_direct_db_clones_operation',
    'description' => t('All'),
  );
  $info['translatables']['path'] = array(
    'name' => t('URL aliases'),
    'operation' => 'translation_clone_direct_db_clones_operation',
    'options' => array('url_alias..alias' => t('Source language paths')),
    'description' => t('Aliases for any new translation entities will be generated as part of their cloning, so this option should be used with care if choosing below to overwrite translations.'),
    'weight' => 5,
  );

  // Field cloners, used by both the entity translation and content translation
  // operations.
  $info['fields'] = array(
    // For example, field collection fields would need deep cloning, as they
    // cannot be shared between entities/translations.
    //'field_collection' => 'translation_clone_field_field_collection',
  );

  return $info;
}
