<?php

/**
 * @file
 * Functionality integrating other contrib modules with Translation Clone.
 */

/**
 * Implements hook_translation_clone_info() on behalf of redirect module.
 */
function redirect_translation_clone_info() {
  $info = array();

  $info['translatables']['redirect'] = array(
    'name' => t('Redirects'),
    'operation' => 'translation_clone_redirect_operation',
    'description' => t('All'),
    // Similar weight to path aliases.
    'weight' => 6,
  );

  return $info;
}

/**
 * Batch operation for cloning redirects.
 *
 * Redirects require unique hashes.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $options
 * @param $context
 */
function translation_clone_redirect_operation($source_language, $target_language, $overwrite, $options, &$context) {
  if (empty($options)) {
    $context['finished'] = 1;
    return;
  }

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'redirect')
    ->propertyCondition('language', $source_language);

  // Record our progress.
  if (empty($context['sandbox'])) {
    $context['sandbox']['entities_step'] = 0;
    $context['sandbox']['entity_max'] = 0;

    $count_query = clone $query;
    $count = $count_query->count()
      ->execute();
    $context['sandbox']['entity_max'] = $count;

    if ($count == 0) {
      $context['finished'] = 1;
      return;
    }
  }

  // Number of entities to be processed for each step.
  $batch_size = variable_get('translation_clone_redirect_batch_size', 30);

  // The entities to process.
  $offset = $context['sandbox']['entities_step'];
  $result = $query->entityOrderBy('entity_id')
    ->range($offset, $batch_size)
    ->execute();

  // Process this batch.
  if (!empty($result['redirect'])) {
    $redirects = entity_load('redirect', array_keys($result['redirect']));
    $entity_info = entity_get_info('redirect');
    foreach ($redirects as $base_redirect) {
      $source_query = isset($base_redirect->source_options['query']) ? $base_redirect->source_options['query'] : array();
      $existing_redirect = redirect_load_by_source($base_redirect->source, $target_language, $source_query);
      if ($overwrite || empty($existing_redirect)) {
        if ($existing_redirect) {
          redirect_delete($existing_redirect->rid);
        }
        // @see entity_ui_clone_entity()
        $new_redirect = clone $base_redirect;

        $new_redirect->{$entity_info['entity keys']['id']} = FALSE;
        if (!empty($entity_info['entity keys']['name'])) {
          $new_redirect->{$entity_info['entity keys']['name']} = FALSE;
        }

        $new_redirect->is_new = TRUE;

        // Make sure the status of a cloned exportable is custom.
        if (!empty($entity_info['exportable'])) {
          $status_key = isset($entity_info['entity keys']['status']) ? $entity_info['entity keys']['status'] : 'status';
          $new_redirect->{$status_key} = ENTITY_CUSTOM;
        }

        $new_redirect->language = $target_language;
        // The is_new property will ensure that a new hash is produced and the
        // count & access properties are reset.
        redirect_save($new_redirect);
      }
      $context['sandbox']['entities_step']++;
    }
  }

  // Report progress.
  $context['finished'] = $context['sandbox']['entities_step'] / $context['sandbox']['entity_max'];
}

/**
 * Implements hook_translation_clone_info() on behalf of Variable translation.
 */
function i18n_variable_translation_clone_info() {
  $info = array();

  $info['translatables']['i18n_variable'] = array(
    'name' => t('Settings'),
    'operation' => 'translation_clone_i18n_variable_operation',
    'description' => t('All translatable variables'),
  );

  return $info;
}

/**
 * Batch operation for cloning translatable variables.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $options
 * @param $context
 */
function translation_clone_i18n_variable_operation($source_language, $target_language, $overwrite, $options, &$context) {
  $schema = drupal_get_schema('variable_store');
  $fields = $schema['fields'];
  unset($fields['realm_key']);
  // Unset any single-field unique key fields.
  foreach (array('unique keys', 'primary key') as $key_type) {
    if (isset($schema[$key_type]) && count($schema[$key_type]) === 1) {
      $field_name = reset($schema[$key_type]);
      unset($fields[$field_name]);
    }
  }
  $fields = array_keys($fields);
  /** @var SelectQuery $from */
  $from = db_select('variable_store', 's')
    ->fields('s', $fields)
    ->condition('s.realm_key', $source_language)
    ->condition('s.realm', 'language');
  $from->addExpression(':language', 'realm_key', array(':language' => $target_language));
  $target_alias = $from->leftJoin('variable_store', 't', 's.realm = %alias.realm AND s.name = %alias.name AND %alias.realm_key = :target', array(':target' => $target_language));
  $target_realm_key_field = $target_alias . '.realm_key';
  $from->isNull($target_realm_key_field);

  if ($overwrite) {
    // Prepare to update any existing items too, which we'll do first so that
    // the original insert query will still only handle items that do not
    // already exist (i.e. including these items that are about to be updated)
    // in the target language.
    $to_update = clone $from;
    $conditions = &$to_update->conditions();
    foreach (array_reverse(element_children($conditions)) as $delta) {
      if ($conditions[$delta]['field'] === $target_realm_key_field) {
        unset($conditions[$delta]);
        $to_update->isNotNull($target_realm_key_field);

        $results = $to_update->execute();
        foreach ($results as $row) {
          drupal_write_record('variable_store', $row, $schema['primary key']);
        }
        break;
      }
    }
  }

  db_insert('variable_store')
    ->fields(array_merge($fields, array('realm_key')))
    ->from($from)
    ->execute();

  // Report progress.
  $context['finished'] = 1;
}

/**
 * Implements hook_translation_clone_info() on behalf of Pathauto module.
 */
function pathauto_translation_clone_info() {
  $info = array();

  $info['translatables']['pathauto'] = array(
    'name' => t('Path alias patterns'),
    'operation' => 'translation_clone_pathauto_operation',
    'description' => t('Content path patterns'),
    // Same weight as path aliases.
    'weight' => 5,
  );

  return $info;
}

/**
 * Batch operation for cloning pathauto patterns.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $options
 * @param $context
 */
function translation_clone_pathauto_operation($source_language, $target_language, $overwrite, $options, &$context) {
  foreach (node_type_get_names() as $node_type => $node_name) {
    $source = variable_get('pathauto_node_' . $node_type . '_' . $source_language . '_pattern', NULL);
    if (!is_null($source)) {

      // Check whether an existing pattern exists.
      if (!$overwrite) {
        $target = variable_get('pathauto_node_' . $node_type . '_' . $target_language . '_pattern', NULL);
        if (!is_null($target)) {
          continue;
        }
      }

      variable_set('pathauto_node_' . $node_type . '_' . $target_language . '_pattern', $source);
    }
  }

  // Report progress.
  $context['finished'] = 1;
}

/**
 * Implements hook_translation_clone_info() on behalf of redirect module.
 */
function mlpanels_translation_clone_info() {
  $info = array();

  $info['translatables']['mlpanels'] = array(
    'name' => t('Multilingual panels'),
    'operation' => 'translation_clone_mlpanels_operation',
    'description' => t('All panes'),
  );

  return $info;
}

/**
 * Batch operation for cloning multilingual panel panes.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $options
 * @param $context
 */
function translation_clone_mlpanels_operation($source_language, $target_language, $overwrite, $options, &$context) {
  if (empty($options)) {
    $context['finished'] = 1;
    return;
  }

  /** @var SelectQuery $query */
  $query = db_select('panels_pane', 'p')
    ->condition('configuration', '%' . db_like('"mlpanels"') . '%', 'LIKE')
    ->condition('configuration', '%' . db_like('"' . $source_language . '"') . '%', 'LIKE');

  // Record our progress.
  if (empty($context['sandbox'])) {
    $context['sandbox']['panes_step'] = 0;
    $context['sandbox']['panes_max'] = 0;

    $count = $query->countQuery()
      ->execute()
      ->fetchField();
    $context['sandbox']['panes_max'] = $count;

    if ($count == 0) {
      $context['finished'] = 1;
      return;
    }
  }

  // Number of panes to be processed for each step.
  $batch_size = variable_get('translation_clone_mlpanels_batch_size', 30);

  // The panes to process.
  $offset = $context['sandbox']['panes_step'];
  $result = $query->fields('p', array())
    ->orderBy('pid')
    ->range($offset, $batch_size)
    ->execute();

  // Process this batch.
  ctools_include('export');
  foreach ($result as $row) {
    $pane = ctools_export_unpack_object('panels_pane', $row);
    if (isset($pane->configuration['mlpanels'][$source_language])) {
      if ($overwrite || !isset($pane->configuration['mlpanels'][$target_language])) {
        $pane->configuration['mlpanels'][$target_language] = $pane->configuration['mlpanels'][$source_language];
        drupal_write_record('panels_pane', $pane, array('pid'));
      }
    }
    $context['sandbox']['panes_step']++;
  }

  // Report progress.
  $context['finished'] = $context['sandbox']['panes_step'] / $context['sandbox']['panes_max'];
}

/**
 * Implements hook_translation_clone_info() on behalf of Custom Breadcrumbs.
 */
function custom_breadcrumbs_translation_clone_info() {
  $info = array();

  $options = array();
  foreach (module_implements('cb_breadcrumb_info') as $module) {
    $bc_info = module_invoke($module, 'cb_breadcrumb_info');
    foreach ($bc_info as $info) {
      $options[$info['table'] . '..paths:' . $info['field']] = $info['type'];
    }
  }
  if ($options) {
    $info['translatables']['custom_breadcrumbs'] = array(
      'name' => t('Custom breadcrumbs'),
      'operation' => 'translation_clone_direct_db_clones_operation',
      'options' => $options,
      // Similar weight to path aliases.
      'weight' => 6,
    );
  }

  return $info;
}

/**
 * Implements hook_translation_clone_info() on behalf of i18n_block.
 */
function i18n_block_translation_clone_info() {
  $info = array();

  $info['translatables']['i18n_block_language'] = array(
    'name' => t('Block languages'),
    'operation' => 'translation_clone_direct_db_clones_operation',
    'description' => t('Visibility settings'),
  );

  return $info;
}
