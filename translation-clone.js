(function ($) {
    Drupal.behaviors.translationClone = {
      attach: function (context, settings) {
        $(context).find('fieldset .form-type-checkboxes > label').once('translation-clone').each(function () {
          $(this).click(function () {
            var checkboxes = $(this).parents('.form-type-checkboxes').find('input[type="checkbox"]');
            var all_checked = true;
            $(checkboxes).each(function () {
              if ($(this).attr('checked') == false) {
                all_checked = false;
              }
            });
            if (!all_checked) {
              $(checkboxes).each(function () {
                $(this).attr('checked', true);
              });
            }
            else {
              $(checkboxes).each(function () {
                $(this).attr('checked', false);
              });
            }

          });
        });

        $(context).find('.translation-clone-select-all').once('translation-clone').each(function () {
          $(this).click(function (e) {
            e.preventDefault();
            var checkboxes = $('#edit-translatables').find('input[type="checkbox"]');
            $(checkboxes).each(function () {
              $(this).attr('checked', true);
            });
          });
        });

        $(context).find('.translation-clone-select-none').once('translation-clone').each(function () {
          $(this).click(function (e) {
            e.preventDefault();
            var checkboxes = $('#edit-translatables').find('input[type="checkbox"]');
            $(checkboxes).each(function () {
              $(this).attr('checked', false);
            });
          });
        });
      }
    }
  })(jQuery);