<?php

/**
 * Admin form callback.
 *
 * @param $form
 * @param $form_state
 */
function translation_clone_admin_form($form, &$form_state) {
  $translation_clone_info = translation_clone_get_info();

  $languages = language_list('enabled');
  $language_options = array();
  foreach ($languages[1] as $lang_code => $properties) {
    $language_options[$lang_code] = $properties->name;
  }

  $form['source_language'] = array(
    '#type' => 'select',
    '#title' => t('Source language'),
    '#options' => $language_options,
    '#required' => TRUE,
    '#description' => t('The language whose content you wish to clone.'),
  );

  $form['target_language'] = array(
    '#type' => 'select',
    '#title' => t('Target language'),
    '#options' => $language_options,
    '#required' => TRUE,
    '#description' => t('The language to clone content into.'),
  );

  $form['translatables'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    //'#title' => t('Types of translatable data to clone'),
  );
  $form['translatables']['selection_helper'] = array(
    '#weight' => -9999,
    '#markup' => t('<p><a class="translation-clone-select-all" href="#">Select all</a> / <a class="translation-clone-select-none" href="#">Select none</a> / Toggle groups by clicking on the group headers.</p>'),
  );
  foreach ($translation_clone_info['translatables'] as $translatable => $translatable_info) {
    $form['translatables'][$translatable] = array(
      '#title' => $translatable_info['name'],
    );
    if (isset($translatable_info['weight'])) {
      $form['translatables'][$translatable]['#weight'] = $translatable_info['weight'];
    }
    $has_description = !empty($translatable_info['description']);
    if (empty($translatable_info['options'])) {
      $description = $has_description ? $translatable_info['description'] : $translatable_info['name'];
      $options = array($translatable => $description);
    }
    else {
      $options = $translatable_info['options'];
      if ($has_description) {
        $form['translatables'][$translatable]['#description'] = $translatable_info['description'];
      }
    }
    $form['translatables'][$translatable] += array(
      '#type' => 'checkboxes',
      '#options' => $options,
    );
  }

  $form['overwrite'] = array(
    '#type' => 'checkbox',
    '#title' => t('Overwrite any existing content/translations in the target language.'),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clone'),
  );

  $form['#attached']['css'][] = drupal_get_path('module', 'translation_clone') . '/translation-clone.css';
  $form['#attached']['js'][]  = drupal_get_path('module', 'translation_clone') . '/translation-clone.js';

  return $form;
}

/**
 * Validation handler.
 *
 * @param $form
 * @param $form_state
 */
function translation_clone_admin_form_validate($form, $form_state) {
  $source_language = $form_state['values']['source_language'];
  $target_language = $form_state['values']['target_language'];
  if ($source_language == $target_language) {
    form_set_error('target_language', t('Please select a different target language to the source language.'));
  }
}

/**
 * Submit handler.
 *
 * @param $form
 * @param $form_state
 */
function translation_clone_admin_form_submit($form, $form_state) {
  $source_language = $form_state['values']['source_language'];
  $target_language = $form_state['values']['target_language'];
  $overwrite       = $form_state['values']['overwrite'];

  // Begin the batch process!
  translation_clone_batch_process_start($source_language, $target_language, $overwrite, $form_state['values']['translatables']);
}
