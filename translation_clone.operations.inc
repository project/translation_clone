<?php

/**
 * Batch operation for cloning translations of entities with entity translation.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $options
 * @param $context
 */
function translation_clone_entity_translations_operation($source_language, $target_language, $overwrite, $options, &$context) {
  if (empty($options)) {
    $context['finished'] = 1;
    return;
  }

  // Record our progress.
  if (empty($context['sandbox'])) {
    $context['sandbox']['bundles_step'] = 0;
    $context['sandbox']['bundles_max'] = count($options);
    $context['sandbox']['entities_step'] = 0;
    $context['sandbox']['entity_max'] = 0;
  }

  $types = array_keys($options);
  $entity_type_and_bundle = $types[$context['sandbox']['bundles_step']];
  list($entity_type, $bundle) = explode(':', $entity_type_and_bundle, 2);

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type)
    ->entityCondition('bundle', $bundle)
    // A hook_query_TAG_alter() in translations_clone.module will join the
    // entity_translation table and add a condition for the source language,
    // once the SelectQuery has been built from the EFQ.
    ->addTag('translation_clone_entity_translations_query')
    ->addMetaData('translation_clone_entity_translations_source_language', $source_language);

  while (empty($context['sandbox']['entity_max']) && $context['sandbox']['bundles_step'] < $context['sandbox']['bundles_max']) {
    $count_query = clone $query;
    $count = $count_query->count()
      ->execute();
    $context['sandbox']['entity_max'] = $count;
    if (empty($count)) {
      $context['sandbox']['bundles_step']++;
      if ($context['sandbox']['bundles_step'] < $context['sandbox']['bundles_max']) {
        $entity_type_and_bundle = $types[$context['sandbox']['bundles_step']];
        list($entity_type, $bundle) = explode(':', $entity_type_and_bundle, 2);
        $query->entityConditions['bundle']['value'] = $bundle;
        $query->entityConditions['entity_type']['value'] = $entity_type;
      }
    }
  }

  if ($context['sandbox']['bundles_step'] < $context['sandbox']['bundles_max']) {
    // Number of entities to be processed for each step.
    $batch_size = variable_get('translation_clone_entity_translation_batch_size', 20);

    // The entities to process.
    $offset = $context['sandbox']['entities_step'];
    $result = $query->entityOrderBy('entity_id')
      ->range($offset, $batch_size)
      ->execute();

    // Process this batch.
    if (!empty($result[$entity_type])) {
      $entities = entity_load($entity_type, array_keys($result[$entity_type]));
      foreach ($entities as $entity) {
        translation_clone_entity_translation($source_language, $target_language, $overwrite, $entity_type, $entity);
        $context['sandbox']['entities_step']++;
      }
    }

    // Report progress.
    $bundles_progress = ($context['sandbox']['bundles_step'] / $context['sandbox']['bundles_max']);
    $entities_progress_within_bundle = ($context['sandbox']['entities_step'] / $context['sandbox']['entity_max']);
    $context['finished'] = $bundles_progress + ($entities_progress_within_bundle / $context['sandbox']['bundles_max']);
    if ($context['sandbox']['entities_step'] == $context['sandbox']['entity_max']) {
      $context['sandbox']['bundles_step']++;
      $context['sandbox']['entity_max'] = 0;
      $context['sandbox']['entities_step'] = 0;
    }
  }
  else {
    // All bundles have been completed or skipped.
    $context['finished'] = 1;
  }
}

/**
 * Performs Translation Clone on entities translated with Entity Translation.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $entity_type
 * @param $entity
 */
function translation_clone_entity_translation($source_language, $target_language, $overwrite, $entity_type, $entity) {
  if ($entity && isset($entity->translations->data[$source_language])) {
    if ($overwrite || !isset($entity->translations->data[$target_language])) {
      $entity->translations->data[$target_language] = $entity->translations->data[$source_language];

      if ($entity->translations->original == $source_language) {
        $entity->translations->data[$target_language]['source'] = $source_language;
      }

      $entity->translations->data[$target_language]['language'] = $target_language;
      $entity->translations->data[$target_language]['changed'] = REQUEST_TIME;
      $entity->translations->data[$target_language]['created'] = REQUEST_TIME;

      list(, , $bundle) = entity_extract_ids($entity_type, $entity);
      $instances = field_info_instances($entity_type, $bundle);
      $field_info_map = field_info_field_map();

      foreach ($instances as $field_name => $field_details) {
        $field_on_entity = $entity->{$field_name};
        $field_type = $field_info_map[$field_name]['type'];
        if (isset($field_on_entity[$source_language])) {
          $field_on_entity[$target_language] = translation_clone_clone_field_translation($field_type, $source_language, $target_language, $field_on_entity[$source_language]);
        }
        $entity->{$field_name} = $field_on_entity;
      }

      $entity->translation_clone = array(
        'source_language' => $source_language,
        'target_language' => $target_language,
        'overwrite' => $overwrite,
      );
      entity_save($entity_type, $entity);
    }
  }
}

/**
 * Batch operation for cloning translations of nodes with content translation.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $options
 * @param $context
 */
function translation_clone_content_translations_operation($source_language, $target_language, $overwrite, $options, &$context) {
  if (empty($options)) {
    $context['finished'] = 1;
    return;
  }

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $options)
    ->propertyCondition('language', $source_language);

  // Record our progress.
  if (empty($context['sandbox'])) {
    $context['sandbox']['entities_step'] = 0;
    $context['sandbox']['entity_max'] = 0;

    $count_query = clone $query;
    $count = $count_query->count()
      ->execute();
    $context['sandbox']['entity_max'] = $count;

    if ($count == 0) {
      $context['finished'] = 1;
      return;
    }
  }

  // Number of entities to be processed for each step.
  $batch_size = variable_get('translation_clone_content_translation_batch_size', 20);

  // The entities to process.
  $offset = $context['sandbox']['entities_step'];
  $result = $query->entityOrderBy('entity_id')
    ->range($offset, $batch_size)
    ->execute();

  // Process this batch.
  if (!empty($result['node'])) {
    $nodes = node_load_multiple(array_keys($result['node']));
    foreach ($nodes as $node) {
      translation_clone_content_translation($source_language, $target_language, $overwrite, $node);
      $context['sandbox']['entities_step']++;
    }
  }

  // Report progress.
  $context['finished'] = $context['sandbox']['entities_step'] / $context['sandbox']['entity_max'];
}

/**
 * Performs Translation Clone on entities translated with Content Translation.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $new_node
 */
function translation_clone_content_translation($source_language, $target_language, $overwrite, $node) {
  // Make a copy, so we keep the old node around.
  $new_node = clone $node;

  // Unset these so that when we save we get a new Node.
  unset($new_node->nid);
  unset($new_node->vid);

  // Check whether there is already a translation in the target language.
  if (module_exists('translation') && translation_supported_type($new_node->type)) {
    if (!empty($new_node->tnid)) {
      $existing_translations = translation_node_get_translations($new_node->tnid);
      if (isset($existing_translations[$target_language])) {
        if (!$overwrite) {
          // We've been told not to overwrite existing translations. Skip.
          return;
        }
        else {
          // "Overwrite" the existing node/translation.
          node_delete($existing_translations[$target_language]->nid);
          // Ensure there is a translation set.
          $new_node->translation_source = $node;
        }
      }
    }
    else {
      // Set up a tnid.
      $new_node->translation_source = $node;
    }
  }

  // Set the new language.
  $new_node->language = $target_language;

  // Translate the fields.
  list(, , $bundle) = entity_extract_ids('node', $new_node);
  $instances = field_info_instances('node', $bundle);
  $field_info_map = field_info_field_map();

  foreach ($instances as $field_name => $field_details) {
    if (!empty($new_node->{$field_name})) {
      $field_on_entity = $new_node->{$field_name};
      $target_field_language = $source_field_language = NULL;
      if (isset($field_on_entity[$source_language])) {
        $source_field_language = $source_language;
        $target_field_language = $target_language;
      }
      elseif (count($field_on_entity) == 1 && isset($field_on_entity[LANGUAGE_NONE])) {
        // The field data should not be language-neutral, but corrupt migrations
        // or other processes may have put it here - allow for this, and just
        // ensure the data is processed by the field translation clone handlers
        // if necessary.
        $target_field_language = $source_field_language = LANGUAGE_NONE;
      }
      if ($source_field_language) {
        $field_type = $field_info_map[$field_name]['type'];
        $field_on_entity[$target_field_language] = translation_clone_clone_field_translation($field_type, $source_language, $target_language, $field_on_entity[$source_field_language]);
        $new_node->{$field_name} = $field_on_entity;
      }
    }
  }

  // Save the node!
  $new_node->translation_clone = array(
    'source_language' => $source_language,
    'target_language' => $target_language,
    'overwrite' => $overwrite,
    'source_entity_id' => $node->nid,
  );
  node_save($new_node);
}

/**
 * Batch operation for cloning translations of locale (interface) strings.
 *
 * This also covers strings done through i18n_string, such as i18n_taxonomy
 * terms that are localizable. It assumes translations are stored in the
 * locales_target table.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $options
 * @param $context
 */
function translation_clone_locale_translations_operation($source_language, $target_language, $overwrite, $options, &$context) {
  if (empty($options)) {
    $context['finished'] = 1;
    return;
  }

  /** @var SelectQuery $query */
  $query = db_select('locales_target', 'lt_source')
    ->fields('lt_source', array())
    ->condition('lt_source.language', $source_language);
  $ls_alias = $query->join('locales_source', 'ls', 'lt_source.lid = %alias.lid');
  $query->condition($ls_alias . '.textgroup', $options);
  $target_alias = $query->leftJoin('locales_target', 'lt_target', 'lt_source.lid = %alias.lid AND %alias.language = :target', array(':target' => $target_language));

  if (!$overwrite) {
    $query->isNull($target_alias . '.language');
  }

  // Record our progress.
  if (empty($context['sandbox'])) {
    // Clone any custom string overrides as a one-off operation.
    foreach (array('locale_custom_strings_', 'locale_custom_disabled_strings_') as $var_prefix) {
      if ($source_overrides = variable_get($var_prefix . $source_language)) {
        $target_overrides = variable_get($var_prefix . $target_language, array());
        foreach ($source_overrides as $context_key => $source_strings) {
          foreach ($source_strings as $source_string => $target_string) {
            if ($overwrite || !isset($target_overrides[$context_key][$source_string])) {
              $target_overrides[$context_key][$source_string] = $target_string;
            }
          }
        }
        variable_set($var_prefix . $target_language, $target_overrides);
      }
    }

    $context['sandbox']['items_step'] = 0;
    $context['sandbox']['items_max'] = 0;

    $count = $query->countQuery()
      ->execute()
      ->fetchField();
    $context['sandbox']['items_max'] = $count;

    if ($count == 0) {
      $context['finished'] = 1;
      return;
    }
  }

  // Number of items to be processed for each step.
  $batch_size = variable_get('translation_clone_locale_translation_batch_size', 50);

  // The items to process.
  $offset = $overwrite ? $context['sandbox']['items_step'] : 0;
  $query->addField($target_alias, 'language', 'target_language');
  $result = $query->orderBy('lt_source.lid')
    ->range($offset, $batch_size)
    ->execute();

  // Process this batch.
  foreach ($result as $row) {
    translation_clone_locale_translation($source_language, $target_language, $overwrite, $row);
    $context['sandbox']['items_step']++;
  }

  // Report progress.
  $context['finished'] = $context['sandbox']['items_step'] / $context['sandbox']['items_max'];
}

/**
 * Performs Translation Clone on locale (interface/i18n_string) strings.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $term
 */
function translation_clone_locale_translation($source_language, $target_language, $overwrite, $row) {
  $row->language = $target_language;
  $primary_keys = empty($row->target_language) ? array() : array(
    'language',
    'lid',
    'plural'
  );
  drupal_write_record('locales_target', $row, $primary_keys);
}

/**
 * Copies the translation of a field value to the new language.
 *
 * @param $field_type
 * @param $source_language
 * @param $target_language
 * @param $source_field
 */
function translation_clone_clone_field_translation($field_type, $source_language, $target_language, $source_field) {
  $translation_clone_info = translation_clone_get_info();

  // Check if there's a function to translate this field.
  if (isset($translation_clone_info['fields'][$field_type])) {
    $function = $translation_clone_info['fields'][$field_type];
    $target_field = $function($source_language, $target_language, $source_field);
  }
  // Else, just clone it.
  else {
    $target_field = $source_field;
  }

  return $target_field;
}

/**
 * Batch operation for cloning translations in arbitrary database tables.
 *
 * @param $source_language
 * @param $target_language
 * @param $overwrite
 * @param $options
 *   An array of database table names, eacg with the name of the language key
 *   column specified after it separated by a dot. For example,
 *   'url_alias.language'. The language column identifier can be omitted, as it
 *   defaults to 'language'.
 * @param $context
 */
function translation_clone_direct_db_clones_operation($source_language, $target_language, $overwrite, $options, &$context) {
  if (empty($options)) {
    $context['finished'] = 1;
    return;
  }

  // Record our progress.
  if (empty($context['sandbox'])) {
    $context['sandbox']['tables_step'] = 0;
    $context['sandbox']['tables_max'] = count($options);
  }

  $tables = array_keys($options);
  $table_option = $tables[$context['sandbox']['tables_step']];

  $table_option = explode('.', $table_option, 3);
  $table = $table_option[0];
  $lang_field = empty($table_option[1]) ? 'language' : $table_option[1];
  $schema = drupal_get_schema($table);
  $fields = $schema['fields'];
  unset($fields[$lang_field]);
  // Unset any single-field unique key fields.
  foreach (array('unique keys', 'primary key') as $key_type) {
    if (isset($schema[$key_type]) && count($schema[$key_type]) === 1) {
      $field_name = reset($schema[$key_type]);
      unset($fields[$field_name]);
    }
  }
  $fields = array_keys($fields);
  /** @var SelectQuery $from */
  $from = db_select($table, 's')
    ->fields('s', $fields)
    ->condition('s.' . $lang_field, $source_language);
  $from->addExpression(':language', $lang_field, array(':language' => $target_language));

  $join_condition = array(
    '%alias.' . $lang_field . ' = :target'
  );
  // Allow specific columns to be joined on, as for example, if the primary key
  // is a serial value, there is no automatic way to decide what to join on
  // instead. This means that is required if there is no primary or unique key.
  if (empty($table_option[2])) {
    $join_on = array();
    foreach (array('unique keys', 'primary key') as $key_type) {
      if (isset($schema[$key_type])) {
        $join_on = array_merge($join_on, array_diff($schema[$key_type], array($lang_field)));
      }
    }
    if (empty($join_on)) {
      throw new Exception(t('No join key supplied to translation clone operation for handling existing values.'));
    }
  }
  else {
    $join_on = explode(':', $table_option[2]);
  }
  foreach ($join_on as $join_field) {
    $join_condition[] = 's.' . $join_field . ' = %alias.' . $join_field;
  }

  $target_alias = $from->leftJoin($table, 't', implode(' AND ', $join_condition), array(':target' => $target_language));
  $target_lang_field = $target_alias . '.' . $lang_field;
  $from->isNull($target_lang_field);

  if ($overwrite) {
    // Prepare to update any existing items too, which we'll do first so that
    // the original insert query will still only handle items that do not
    // already exist (i.e. including these items that are about to be updated)
    // in the target language.
    $to_update = clone $from;
    $conditions = &$to_update->conditions();
    foreach (array_reverse(element_children($conditions)) as $delta) {
      if ($conditions[$delta]['field'] === $target_lang_field) {
        unset($conditions[$delta]);
        $to_update->isNotNull($target_lang_field);

        foreach (array('unique keys', 'primary key') as $key_type) {
          if (isset($schema[$key_type]) && count($schema[$key_type]) === 1) {
            $field_name = reset($schema[$key_type]);
            if ($field_name != $lang_field) {
              $to_update->addField($target_alias, $field_name);
            }
          }
        }

        $results = $to_update->execute();
        foreach ($results as $row) {
          drupal_write_record($table, $row, $schema['primary key']);
        }
        break;
      }
    }
  }

  db_insert($table)
    ->fields(array_merge($fields, array($lang_field)))
    ->from($from)
    ->execute();

  $context['sandbox']['tables_step']++;

  // Report progress.
  $context['finished'] = $context['sandbox']['tables_step'] / $context['sandbox']['tables_max'];
}
